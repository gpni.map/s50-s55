import Banner from '../components/Banner';


export default function Error404(){

	const data = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for does not exist.",
		destination: "/",
		label: "Retun to Homepage"
	}


	return(
		<Banner dataProp={data}/>
	)
}