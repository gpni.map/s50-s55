import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//import AppNavbar from './AppNavbar';


// Import the React Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*const name = "Glenn Perey";

const student = {
  firstName: "Myrtle",
  lastName: "Perey"
}


function  username(user) {
  return user.firstName + ' ' + user.lastName
}


const element = <h1>Hello, {username(student)}</h1>


root.render(element)*/