import { Accordion, Button } from 'react-bootstrap';

export default function Resources () {
	return (
		<Accordion>
	      <Accordion.Item eventKey="0">
	        <Accordion.Header>React Bootstrap Accordion</Accordion.Header>
	        <Accordion.Body>
	          Build vertically collapsing accordions in combination with the Collapse component. 
	          <Button className="btnResources" href="https://react-bootstrap.github.io/components/accordion/" target="_blank" variant="primary">Read more</Button>
	        </Accordion.Body>
	      </Accordion.Item>
	      <Accordion.Item eventKey="1">
	        <Accordion.Header>Rendering Elements</Accordion.Header>
	        <Accordion.Body>
	          Elements are the items you want to display on your browser or page. To interpret the elements in your page,
	          you will have to render it. 
	          <Button className="btnResources" href="https://reactjs.org/docs/rendering-elements.html" target="_blank" variant="primary">Read more</Button>
	        </Accordion.Body>
	      </Accordion.Item>
	      <Accordion.Item eventKey="2">
	        <Accordion.Header>ReactJS Components</Accordion.Header>
	        <Accordion.Body>
	          A Component is one of the core building blocks of React. In other words, we can say that every 
	          application you will develop in React will be made up of pieces called components. 
	          Components make the task of building UIs much easier. 
	          <Button className="btnResources" href="https://www.geeksforgeeks.org/reactjs-components/" target="_blank" variant="primary">Read more</Button>
	        </Accordion.Body>
	      </Accordion.Item>
	    </Accordion>
	)
}